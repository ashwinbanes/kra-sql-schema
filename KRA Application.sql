create database KRA;
use KRA;

create table ROLE
(
	ROLE_ID bigint auto_increment not null primary key,
    NAME varchar(30) not null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table DESIGNATION
(
	DESIGNATION_ID bigint auto_increment not null primary key,
    NAME varchar(30) not null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table STREAM
(
	STREAM_ID bigint auto_increment not null primary key,
    NAME varchar(30) not null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table KRA_DESCRIPTION
(
	KRA_DESCRIPTION_ID bigint auto_increment not null primary key,
    KRA varchar(5000) null,
    SELF_GYAAN varchar(5000) null,
    PRIORITY int null,
    WAY_FORWARD varchar(5000) null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table STATUS
(
	STATUS_ID bigint auto_increment not null primary key,
    NAME varchar(30) not null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table QUARTER_YEAR
(
	QUARTER_YEAR_ID bigint auto_increment not null primary key,
    QUARTER varchar(30) not null,
    YEAR int not null,
    DEADLINE_DATE datetime null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);

create table QUARTER_MONTH
(
	QUARTER_MONTH_ID bigint auto_increment not null primary key,
    QUARTER varchar(30) not null,
    MONTH varchar(30) not null,
    SEQUENCE int null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null
);


create table EMPLOYEE
(
	EMPLOYEE_ID bigint auto_increment not null primary key,
    NAME varchar(30) not null,
    EMAIL varchar(100) not null,
    PASSWORD_HASH varchar(5000) null,
    DESIGNATION_ID bigint null,
    ROLE_ID bigint null,
    STREAM_ID bigint null,
    IS_ACTIVE bit null,
    IS_MENTOR bit null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(DESIGNATION_ID) references DESIGNATION(DESIGNATION_ID),
    foreign key(ROLE_ID) references ROLE(ROLE_ID),
    foreign key(STREAM_ID) references STREAM(STREAM_ID)
);

create table MENTOR_MENTEE
(
	MENTOR_ID bigint not null,
    MENTEE_ID bigint not null,
    primary key(MENTOR_ID, MENTEE_ID),
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(MENTOR_ID) references EMPLOYEE(EMPLOYEE_ID),
    foreign key(MENTEE_ID) references EMPLOYEE(EMPLOYEE_ID)
);

create table KRA_EMPLOYEE
(
	KRA_SET_ID bigint auto_increment not null primary key,
    EMPLOYEE_ID bigint not null,
    QUARTER_YEAR_ID bigint not null,
    MENTOR_ID bigint not null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(EMPLOYEE_ID) references EMPLOYEE(EMPLOYEE_ID),
    foreign key(QUARTER_YEAR_ID) references QUARTER_YEAR(QUARTER_YEAR_ID),
    foreign key(MENTOR_ID) references EMPLOYEE(EMPLOYEE_ID)
);

create table KRA_SET_KRA
(
	KRA_SET_ID bigint not null,
    KRA_DESCRIPTION_ID bigint not null,
    CREATED_DATE timestamp not null,
    STATUS_ID bigint null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(KRA_SET_ID) references KRA_EMPLOYEE(KRA_SET_ID),
    foreign key(STATUS_ID) references STATUS(STATUS_ID),
    foreign key(KRA_DESCRIPTION_ID) references KRA_DESCRIPTION(KRA_DESCRIPTION_ID)
);

create table PROJECTS
(
	PROJECT_ID bigint auto_increment not null primary key,
    PROJECT_NAME varchar(30) not null,
    IS_ACTIVE bit null,
    MANAGER_ID bigint null,
    FROM_DATE datetime null,
    TO_DATE datetime null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(MANAGER_ID) references EMPLOYEE(EMPLOYEE_ID)
);

create table EMPLOYEE_PROJECT
(
	EMPLOYEE_ID bigint not null,
    PROJECT_ID bigint not null,
    primary key(EMPLOYEE_ID, PROJECT_ID),
    IS_ACTIVE bit null,
    FROM_DATE datetime null,
    TO_DATE datetime null,
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(EMPLOYEE_ID) references EMPLOYEE(EMPLOYEE_ID),
    foreign key(PROJECT_ID) references PROJECTS(PROJECT_ID)
);

create table PROJECTS_STREAM
(
	PROJECT_ID bigint not null,
    STREAM_ID bigint not null,
    primary key(PROJECT_ID, STREAM_ID),
    CREATED_DATE timestamp not null,
    UPDATED_DATE datetime null,
    CREATED_BY int null,
    UPDATED_BY int null,
    foreign key(PROJECT_ID) references PROJECTS(PROJECT_ID),
    foreign key(STREAM_ID) references STREAM(STREAM_ID)
);